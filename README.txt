The private space module allows to create a "private space" for each
user in the form of a node that only they have access to. Administrators
provide a template node, and private nodes are created on the fly when
first accessed. Features include :

* Any node can be used as template for private spaces

* Unique, configurable access point. When going to (eg.) /privatespace
each user is redirected to his or her own private space

* Attempt to access private space when not logged in can redirect to a
specified node (eg. login page)

* The private space can be added (or not) automatically to the
navigation menu

* Administration interface shows each user's private space

* Access rights can be strictly enforced via the node api (for 4.7).

* Provided API allows module creators to manage access rights
themselves
